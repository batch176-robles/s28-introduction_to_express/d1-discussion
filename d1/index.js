// [OBJECTIVE] Create a server-side app using Express Web Framework


// Relate this task to something that you do on a daily basis.

// [Section] append the entire app to our node package manager.

// package.json -> the "heart" of every node project. This also contains different metadata that describes the structure of the project.

// scripts -> is used to declare and describe custom commands and keyword that can be used to execute this project with the correct runtime environment.

// NOTE: "start" is globally recognize amongst node projects and frameworks as the 'default' command script to execute a task/project.

// however for *unconventional* keywords or command you have to append the command "run" 

// SYNTAX:

// 1. Identify and prepare the ingredients.
const express = require("express")

// express => will be used as the main component to create the server.

// We need to be able to gather/acquire the utilities and components needed that the express library will provide us.

// => require() -> directive used 

// prepare the environment in which the project will be served.


// [SECTION] Preparing a Remote repository for our Node project.

// NOTE: always DISABLE the node_modules folder
// WHY? 
// -> it will take up too much space in our repository and making it a lot more difficult to stage upon commiting the changes in our remote repository.

// -> if ever that you will deploy your node project on deployment platforms (heroku, netlify, vercel) the project will automatically be rejected because node_modules is not recognized on various deployment platforms.

// HOW? using a .gitignore module

// [SECTION] Create a Runtime environment that will automatically fix all the changes in our app.
// We're going to use a utility called nodemon.
// upon starting the entry point module with nodemon you will be able to the 'append' the application with the proper run time environment. Allowing you to save time and effort upon commiting changes to your app.

// you can even insert items like text art into your run time environment.
console.log(`
Welcome to our Express API Server
────────────────────▄▄▄▄
────────────────▄▄█▀▀──▀▀█▄
─────────────▄█▀▀─────────▀▀█▄
────────────▄█▀──▄▄▄▄▄▄──────▀█
────────────█───█▌────▀▀█▄─────█
────────────█──▄█────────▀▀▀█──█
────────────█──█──▀▀▀──▀▀▀▄─▐──█
────────────█──▌────────────▐──█
────────────█──▌─▄▀▀▄───────▐──█
───────────█▀▌█──▄▄▄───▄▀▀▄─▐──█
───────────▌─▀───█▄█▌─▄▄▄────█─█
───────────▌──────▀▀──█▄█▌────█
───────────█───────────▀▀─────▐
────────────█──────▌──────────█
────────────██────█──────────█
─────────────█──▄──█▄█─▄────█
─────────────█──▌─▄▄▄▄▄─█──█
─────────────█─────▄▄──▄▀─█
─────────────█▄──────────█
─────────────█▀█▄▄──▄▄▄▄▄█▄▄▄▄▄
───────────▄██▄──▀▀▀█─────────█
──────────██▄─█▄────█─────────█
───▄▄▄▄███──█▄─█▄───█─────────██▄▄▄
▄█▀▀────█────█──█▄──█▓▓▓▓▓▓▓▓▓█───▀▀▄
█──────█─────█───████▓▓▓▓▓▓▓▓▓█────▀█
█──────█─────█───█████▓▓▓▓▓▓▓█──────█
█─────█──────█───███▀▀▀▀█▓▓▓█───────█
█────█───────█───█───▄▄▄▄████───────█
█────█───────█──▄▀───────────█──▄───█
█────█───────█─▄▀─────█████▀▀▀─▄█───█
█────█───────█▄▀────────█─█────█────█
█────█───────█▀───────███─█────█────█
█─────█────▄█▀──────────█─█────█────█
█─────█──▄██▀────────▄▀██─█▄───█────█
█────▄███▀─█───────▄█─▄█───█▄──█────█
█─▄██▀──█──█─────▄███─█─────█──█────█
██▀────▄█───█▄▄▄█████─▀▀▀▀█▀▀──█────█
█──────█────▄▀──█████─────█────▀█───█
───────█──▄█▀───█████─────█─────█───█
──────▄███▀─────▀███▀─────█─────█───█
─────────────────────────────────────
▀█▀─█▀▄─█─█─█▀────▄▀▀─▀█▀─▄▀▄─█▀▄─█─█
─█──█▄▀─█─█─█▀────▀▀█──█──█─█─█▄▀─█▄█
─▀──▀─▀─▀▀▀─▀▀────▀▀───▀───▀──▀─▀─▄▄█
─────────────────────────────────────
`)

